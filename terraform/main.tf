provider "azurerm" {
  features {}
  client_id       = "048fc998-3940-4e53-a328-e8a613b9ffc9"
  client_secret   = "bO68Q~R5yd_991od6JW6~EpFklnhqSTPBuzZgadR"
  tenant_id       = "0b578d97-bb35-4c75-863a-9280b97ff86e"
  subscription_id = "73e52e62-c4bf-49a5-8caf-3e0da9b19ad8"
}

resource "azurerm_resource_group" "aks_resource" {
  name     = "example-resources"
  location = "Central US"
}

resource "azurerm_kubernetes_cluster" "aks_resource" {
  name                = "aks1"
  location            = azurerm_resource_group.aks_resource.location
  resource_group_name = azurerm_resource_group.aks_resource.name
  dns_prefix          = "exampleaks1"

  default_node_pool {
    name       = "default"
    node_count = 1
    vm_size    = "Standard_D4ds_v5"  
  }

  identity {
    type = "SystemAssigned"
  }

  tags = {
    Environment = "Production"
  }
}

output "client_certificate" {
  value     = azurerm_kubernetes_cluster.aks_resource.kube_config[0].client_certificate
  sensitive = true
}

output "kube_config" {
  value     = azurerm_kubernetes_cluster.aks_resource.kube_config_raw
  sensitive = true
}
